<div class="container">
    <div class="row">
        <div class="col-12 col-lg-6 m-auto">
            <h1>Login</h1>
            <form action="" method="post" class="my-3">
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="text" name="email" id="email" aria-describedby="emailHelp"
                           class="form-control <?= $error['email']? 'is-invalid':''?>">
                    <div class="invalid-feedback"><?= $error['email'][0]?? '' ?></div>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password"
                           class="form-control  <?= $error['password']? 'is-invalid':''?>">
                    <div class="invalid-feedback"><?= $error['password'][0]?? '' ?></div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
