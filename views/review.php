<?php
use app\core\Application;
?>
<h1>Review page</h1>
<div class="container mb-3">
    <div class="row mt-5">
        <div class="col-12 col-md-5 col-lg-5 m-auto">
            <div class="card p-2 w-100">
                <img class="card-img-top" src="uploads/<?= $product['image'] ?>" width="100%" height="250px" alt="img">
                <div class="card-body mt-2 text-center bg-card">
                    <h5 class="card-title"><?= $product['name'] ?></h5>
                    <p class="card-text"><?= $product['description'] ?></p>
                </div>
            </div>
        </div>
    </div>
</div>
    <?php if(Application::isGuest()): ?>
    <h5 class="text-center bg-secondary text-warning"> You haven't logged into your account</h5>
    <?php else:?>
<div class="container mt-2 bg-secondary p-3">
    <form action=" " method="post">
        <div class="row justify-content-center bg-secondary p-2">
            <div class="col-8 col-lg-8">
                <label for="comment" class="form-label">Leave a comment</label>
                <textarea class="form-control" id="comment" name="comment_text" rows="4" required></textarea>
            </div>
            <div class=" col-3 col-lg-2 ">
                <div class="rate mt-1">
                    <?php for($i=1;$i<=5;$i++):?>
                    <input type="radio" id="star<?=$i?>" name="stars" value="<?=$i?>" hidden required>
                    <label for="star<?=$i?>"><i class="bi bi-star-fill"></i> <?=$i?></label><br>
                    <?php endfor ?>
                </div>
            </div>
            <button type="submit" class="btn btn-warning h-100 mt-auto">Make review</button>
        </div>
    </form>
</div>
<?php endif?>