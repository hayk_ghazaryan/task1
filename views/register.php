<div class="container">
    <div class="row">
        <div class="col-12 col-lg-6 m-auto">
            <h1>Create an account</h1>
            <form action="" method="post" class="my-3">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="firstname">Firstname</label>
                            <input type="text" name="firstname" id="firstname"
                                   class="form-control <?= $error['firstname']? 'is-invalid':''?>">
                            <div class="invalid-feedback"><?= $error['firstname'] ?></div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="lastname">Lastname</label>
                            <input type="text" name="lastname" id="lastname"
                                   class="form-control <?= $error['lastname']? 'is-invalid':''?>">
                            <div class="invalid-feedback"><?= $error['lastname']?? '' ?></div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="age">Age</label>
                            <input type="number" name="age" id="age"
                                   class="form-control <?= $error['age']? 'is-invalid':''?>">
                            <div class="invalid-feedback"><?= $error['age'] ?></div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="text" name="email" id="email" aria-describedby="emailHelp"
                           class="form-control <?= $error['email']? 'is-invalid':''?>">
                    <div class="invalid-feedback"><?= $error['email']?? '' ?></div>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password"
                           class="form-control <?= $error['password']? 'is-invalid':''?>">
                    <div class="invalid-feedback"><?= $error['password']?? '' ?></div>
                </div>
                <div class="form-group">
                    <label for="password2">Confirm Password</label>
                    <input type="password" name="confirmPassword" id="password2"
                           class="form-control <?= $error['confirmPassword']? 'is-invalid':''?>">
                    <div class="invalid-feedback"><?= $error['confirmPassword']?? '' ?></div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>