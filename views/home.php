<h1>Home</h1>
<div class="container">
    <div class="row justify-content-between mt-5">
        <?php $i = 0; foreach ($products as $product):?>
        <div class="col-12 col-md-6 col-lg-4 ">
            <div class="card p-2 w-100">
                <img class="card-img-top" src="uploads/<?= $product['image']?>" width="100%" alt="img">
                <div class="card-body bg-card">
                    <h5 class="card-title"><?= $product['name'] ?></h5>
                    <p class="card-text"><?= $product['description'] ?></p>
                       <p>Total reviews: <sup><?=$stars[$i]['count']?></sup></p>
                        <p class='d-inline'>Rating:</p>
                    <div class="all-stars" style="background-size:<?= $stars[$i]['avg']*20 ?>%, 0;">
                        <i class="bi bi-star-fill"></i>
                        <i class="bi bi-star-fill"></i>
                        <i class="bi bi-star-fill"></i>
                        <i class="bi bi-star-fill"></i>
                        <i class="bi bi-star-fill"></i>
                    </div> 
                    <p>5 / <?= $stars[$i++]['avg']?? 0 ?></p>
                    <a href="/review?name=<?= $product['name'] ?>&id=<?=$product['id']?>" class="btn btn-primary" role="button">Review</a>
                </div>
            </div>
        </div>
        <?php endforeach;?>
    </div>
</div>