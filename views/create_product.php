<div class="container">
    <div class="row">
        <div class=" col-12 col-lg-6 m-auto">
            <h1>Create new product</h1>
            <form action="" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Product Name</label>
                    <input type="text" name="name"  id="name"
                     class="form-control <?= $errors['name']? 'is-invalid':''?>">
                    <div class="invalid-feedback"><?= $errors['name']?? '' ?></div>
                </div>
                <div class="form-group">
                    <label for="description">Product Description</label>
                    <input type="text" name="description" id="description"
                     class="form-control <?= $errors['description']? 'is-invalid':''?>">
                      <div class="invalid-feedback"><?= $errors['description']?? '' ?></div>
                </div>
                <div class="form-group">
                  <div class="custom-file">
                        <input type="file" class="custom-file-input first" id="image" name="image" required>
                        <label class="custom-file-label" for="image">Choose Product image</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
     let fileName = document.getElementById("image");
     fileName.addEventListener('change', function(e) {
       fileName = document.getElementById("image").files[0].name;
        let next = e.target.nextElementSibling;
        next.innerText = fileName;
});
</script>