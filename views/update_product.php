<div class="container">
    <div class="row">
        <div class=" col-12 col-lg-6 m-auto">
            <h1 class="text-center mb-3">Update Product</h1>
            <form action="" method="post" enctype="multipart/form-data">
            <div class="border">
                <img src="../uploads/<?= $product['image']?>" width="100%" height="200px" alt="img">
            </div>
                <div class="form-group">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input first" id="image" name="image"
                        required>
                        <label class="custom-file-label" for="image">Choose Product image</label>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name">Product Name</label>
                    <input type="text" name="name"  id="name" value="<?= $product['name'] ?>"
                     class="form-control <?= $errors['name']? 'is-invalid':''?>">
                    <div class="invalid-feedback"><?= $errors['name']?? '' ?></div>
                </div>
                <div class="form-group">
                    <label for="description">Product Description</label>
                    <input type="text" name="description" id="description"
                    value="<?= $product['description']?>"
                     class="form-control <?= $errors['description']? 'is-invalid':''?>">
                      <div class="invalid-feedback"><?= $errors['description']?? '' ?></div>
                </div>
                <button type="submit" class="btn btn-primary">
                    Update <i class="bi bi-pencil-square"></i>
                </button>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
     let fileName = document.getElementById("image");
     fileName.addEventListener('change', function(e) {
       fileName = document.getElementById("image").files[0].name;
        let next = e.target.nextElementSibling;
        next.innerText = fileName;
});
</script>