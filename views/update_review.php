<div class="container">
    <div class="row">
        <div class="col-12 col-lg-6 m-auto">
            <h1>Add reviews</h1>
            <form action=" " method="post">
                <div class="form-group">
                    <label for="name">User Name</label>
                    <input type="text" name="user_name"  id="name" value="<?= $product['user_name']?>"
                           class="form-control <?= $errors['user_name']? 'is-invalid':''?>">
                    <div class="invalid-feedback"><?= $errors['user_name']?? '' ?></div>
                </div>
                <div class="form-group">
                    <label for="product">Product Name</label>
                    <input type="text" name="product_name"  id="product" value="<?= $product['product_name']?>"
                           class="form-control <?= $errors['product_name']? 'is-invalid':''?>">
                    <div class="invalid-feedback"><?= $errors['product_name']?? '' ?></div>
                </div>
                <div class="form-group w-25 ml-auto">
                    <label for="user_id">User_id</label>
                    <input type="number" name="user_id" id="user_id" min="1" value="<?=$product['user_id']?>"
                             class="form-control <?= $errors['user_id']? 'is-invalid':''?>">
                    <div class="invalid-feedback"><?= $errors['user_id']?? '' ?></div>
                </div>
                <div class="row justify-content-center bg-secondary p-2">
                    <div class="col-8 col-lg-8">
                        <label for="comment" class="form-label">Leave a comment</label>
                        <textarea class="form-control" id="comment" name="comment_text" rows="4"><?= $product['comment_text']?></textarea>
                        <small class=" bg-warning "><?= $errors['comment_text']??''?></small>
                    </div>
                    <div class=" col-3 col-lg-2 ">
                        <div class="rate mt-1">
                            <?php for($i=1;$i<=5;$i++):?>
                                <input type="radio" id="star<?=$i?>" name="stars" value="<?=$i?>" hidden required <?=($product['stars']==$i)?'checked':'' ?>>
                                <label for="star<?=$i?>"><i class="bi bi-star-fill"></i> <?=$i?></label><br>
                            <?php endfor ?>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-warning h-100">
                        Update <i class="bi bi-pencil-square"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

