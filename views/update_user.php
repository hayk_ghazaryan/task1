<div class="container">
    <div class="row">
        <div class="col-12 col-lg-6 m-auto">
            <h1>Update User</h1>
            <form action="" method="post" class="my-3">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="firstname">Firstname</label>
                            <input type="text" name="firstname" id="firstname" value="<?=$product['firstname']?>"
                                   class="form-control <?= $errors['firstname']? 'is-invalid':''?>">
                            <div class="invalid-feedback"><?= $errors['firstname'] ?></div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="lastname">Lastname</label>
                            <input type="text" name="lastname" id="lastname" value="<?=$product['lastname']?>"
                                   class="form-control <?= $errors['lastname']? 'is-invalid':''?>">
                            <div class="invalid-feedback"><?= $errors['lastname']?? '' ?></div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="age">Age</label>
                            <input type="number" name="age" id="age" min="0" max="99" value="<?=$product['age']?>"
                                   class="form-control <?= $errors['age']? 'is-invalid':''?>">
                            <div class="invalid-feedback"><?= $errors['age'] ?></div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="status">Status</label>
                            <input type="number" name="status" id="status" min="0" max="1" required value="<?=$product['status']?>"
                                   class="form-control <?= $errors['status']? 'is-invalid':''?>">
                            <div class="invalid-feedback"><?= $errors['status'] ?></div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="text" name="email" id="email" aria-describedby="emailHelp" value="<?=$product['email']?>"
                           class="form-control <?= $errors['email']? 'is-invalid':''?>" readonly>
                    <div class="invalid-feedback"><?= $errors['email']?? '' ?></div>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password" value="<?=$product['password']?>"
                           class="form-control <?= $errors['password']? 'is-invalid':''?>"readonly>
                    <div class="invalid-feedback"><?= $errors['password']?? '' ?></div>
                </div>
                <div class="form-group">
                    <label for="password2">Confirm Password</label>
                    <input type="password" name="confirmPassword" id="password2" value="<?=$product['password']?>"
                           class="form-control <?= $errors['confirmPassword']? 'is-invalid':''?>"readonly>
                    <div class="invalid-feedback"><?= $errors['confirmPassword']?? '' ?></div>
                </div>
                <button type="submit" class="btn btn-primary">
                    Update<i class="bi bi-pencil-square"></i>
                </button>
            </form>
        </div>
    </div>
</div>