<?php
use app\core\Application;

?>
<!doctype html>
<html lang="en">
<head>
   <?php include('header.php') ?>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="#">Task</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                </li>
            </ul>
            <?php if(Application::isGuest()): ?>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/login">Sing in</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/register">Sing up</a>
                </li>
            </ul>
            <?php else:?>
                <div class="btn-group ml-auto">
                    <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?= Application::$app->user->getDisplayName() ?>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="/profile?category=user">Profile <i class="bi bi-person-check"></i></a>
                        <a class="dropdown-item" href="/logout">Logout <i class="bi bi-box-arrow-right"></i></a>
                    </div>
                </div>
            <?php endif;?>
        </div>
    </nav>
    <main>
        <div class="container">
            <?php if(Application::$app->session->getFlash('success')):?>
            <div class="alert alert-success">
                <?php echo Application::$app->session->getFlash('success'); ?>
            </div>
            <?php endif ?>
            {{content}}
        </div>
    </main>
</body>
</html>