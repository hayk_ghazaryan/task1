<div class="container">
    <div class="row">
        <div class="col-12 col-lg-6 m-auto">
            <h1>Create new user</h1>
            <form action="" method="post" class="my-3">
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label for="firstname">Firstname</label>
                            <input type="text" name="firstname" id="firstname"
                                   class="form-control <?= $errors['firstname']? 'is-invalid':''?>">
                            <div class="invalid-feedback"><?= $errors['firstname'] ?></div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="lastname">Lastname</label>
                            <input type="text" name="lastname" id="lastname"
                                   class="form-control <?= $errors['lastname']? 'is-invalid':''?>">
                            <div class="invalid-feedback"><?= $errors['lastname']?? '' ?></div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="age">Age</label>
                            <input type="number" name="age" id="age" min="0" max="99"
                                   class="form-control <?= $errors['age']? 'is-invalid':''?>">
                            <div class="invalid-feedback"><?= $errors['age'] ?></div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="form-group">
                            <label for="status">Status</label>
                            <input type="number" name="status" id="status" min="0" max="1" required class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="text" name="email" id="email" aria-describedby="emailHelp"
                           class="form-control <?= $errors['email']? 'is-invalid':''?>">
                    <div class="invalid-feedback"><?= $errors['email']?? '' ?></div>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" id="password"
                           class="form-control <?= $errors['password']? 'is-invalid':''?>">
                    <div class="invalid-feedback"><?= $errors['password']?? '' ?></div>
                </div>
                <div class="form-group">
                    <label for="password2">Confirm Password</label>
                    <input type="password" name="confirmPassword" id="password2"
                           class="form-control <?= $errors['confirmPassword']? 'is-invalid':''?>">
                    <div class="invalid-feedback"><?= $errors['confirmPassword']?? '' ?></div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>