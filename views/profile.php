<?php
use app\core\Application;
?>

<?php if(Application::$app->session->get('role')): ?>
<h1>Admin Panel</h1>
  <?php if(Application::$app->session->getFlash('delete')||
        Application::$app->session->getFlash('create')||
        Application::$app->session->getFlash('create')):?>
        <div class="alert alert-primary text-center">
            <?php echo Application::$app->session->getFlash('delete'); ?>
            <?php echo Application::$app->session->getFlash('create'); ?>
            <?php echo Application::$app->session->getFlash('update'); ?>
        </div>
    <?php endif ?>
<a href="/create/user" role="button" class="btn btn-primary my-3">Add users</a>
<a href="/create/product" role="button" class="btn btn-primary my-3">Add products</a>
<a href="/create/review" role="button" class="btn btn-primary my-3">Add reviews</a>
<div class="container">
    <div class="row">
        <div class="col-4 col-lg-2">
            <div class="sidebar bg-warning border">
                <ul class="navbar-nav p-2 text-center">
                    <li class="nav-link border">
                        <i class="bi bi-person-rolodex" style="font-size:40px"></i>
                    </li>
                    <li class="nav-link border">
                        <a href="profile?category=user">User</a>
                    </li>
                    <li class="nav-link border">
                        <a href="profile?category=product">Products</a>
                    </li>
                    <li class="nav-link border">
                        <a href="profile?category=review">Reviews</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class=" col-12 col-lg-10 mb-2">
            <table class="table display text-center" id ="myTable">
                <thead>
                    <tr>
                        <th>#</th>
                            <?php foreach($products[0] as $key => $product):?>
                                <th scope="col"><?= $key ?></th>
                            <?php endforeach;?>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0; foreach($products as $elem):?>
                        <tr>
                            <td class="bg-secondary"><?= ++$i ?></td>
                             <?php foreach($elem as $product):?>
                                 <td><?= $product ?></td>
                             <?php endforeach;?>
                            <td>
                                <a href="/profile/update?category=<?=$_GET['category']?>&id=<?=$elem['id'] ?>"
                                   class="btn btn-primary">Update</a>
                                
                            </td>
                            <td>
                                <a href="/profile/delete?category=<?=$_GET['category']?>&id=<?=$elem['id'] ?>"
                                   class="btn btn-danger">Delete</a>
                            </td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<?php else: echo "Congrats <b><u> ". Application::$app->user->getDisplayName() .
    " </u></b>Now you can leave a comments"; ?>
<?php endif ?>