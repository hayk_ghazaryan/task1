<?php
namespace app\core;

use app\models\User;

class Application
{
    public static string $ROOT_DIR;
    public string $userClass;
    public static Application $app;
    public Router $router;
    public Request $request;
    public Response $response;
    public Session $session;
    public Database $db;
    public ?DbModel $user = null;

    public function __construct($rootPath,$config)
    {
        $this->userClass = $config['userClass'];
        self::$ROOT_DIR = $rootPath;
        self::$app = $this;
        $this->request = new Request();
        $this->response = new Response();
        $this->session = new Session();
        $this->router = new Router($this->request,$this->response);

        $this->db = new Database($config['db']);

        $primaryValue = $this->session->get('user');
        if($primaryValue){
            $primaryKey = $this->userClass::primaryKey();
            $this->user = $this->userClass::findOne([$primaryKey => $primaryValue]);
        }
        else{
            $this->user=null;
        }

    }

    public function run()
    {
        try{
            echo  $this->router->resolve();
        }catch (\Exception $e){
            $this->response->setStatusCode(403);
            echo $this->router->renderView('_error',[
                'exception' => $e
            ]);
        }

    }

    public function login(DbModel $user)
    {
        $this->user = $user;
        $primaryKey = $user->primaryKey();
        $primaryValue = $user->{$primaryKey};
        $roleKey = $user->role();
        $roleValue = $user->{$roleKey};
        $actionKey = $user->user();
        $actionValue = $user->{$actionKey};

        $this->session->set('user', $primaryValue);
        $this->session->set('role', $roleValue);
        $this->session->set('action', $actionValue);
        return true;
    }

    public function logout()
    {
        $this->session->remove('user');
        $this->session->remove('role');
        $this->session->remove('action');
    }

    public static function isGuest()
    {
        return !self::$app->user;
    }

}