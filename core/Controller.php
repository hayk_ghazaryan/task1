<?php

namespace app\core;

use app\core\middlewares\BaseMiddleware;

class Controller
{
    public string $actions ='';
    /**
    @var \app\core\middlewares\BaseMiddleware[];
    */
    protected array $middlewares = [];

    public function render($view, $params = [])
    {
        return Application::$app->router->renderView($view,$params);
    }

    public function registerMiddleware(BaseMiddleware $middleware)
    {
        $this->middlewares[] = $middleware;
    }

    /**
     * @return array
     */
    public function getMiddlewares(): array
    {
        return $this->middlewares;
    }

}