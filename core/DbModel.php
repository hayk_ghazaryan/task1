<?php

namespace app\core;

abstract class DbModel extends Model
{
    abstract public function tableName():string;

    abstract public function attributes():array;

    abstract public function primaryKey():string;

        public function save()
        {
            $tableName = $this->tableName();
            $attributes = $this->attributes();
            $params = array_map(fn($attr) => ":$attr",$attributes);
            $statement = self::prepare("INSERT INTO $tableName(".implode(',', $attributes).")
                VALUES(".implode(',',$params).")");

            foreach($attributes as $attribute){
                $statement->bindValue(":$attribute", $this->{$attribute});
            }


            $statement->execute();
            return true;
        }

        public  function findOne($where)
        {
            $tableName = static::tableName();
            $attributes = array_keys($where);
            $sql = implode(' AND ', array_map(fn($attr) => "$attr = :$attr", $attributes));
            $statement = self::prepare("SELECT * FROM $tableName WHERE $sql");
            foreach($where as $key => $item){
                $statement->bindValue(":$key",$item);

            }

            $statement->execute();
            return $statement->fetchObject(static::class);

        }

        public function all()
        {
            $tableName = $this->tableName();
            $statement = self::prepare("SELECT * FROM $tableName");
             $statement->execute();
              return $statement->fetchAll(\PDO::FETCH_ASSOC);

        }

    public function getAvg()
    {
        $tableName = $this->tableName();
        $statement = self::prepare("SELECT products.id,COUNT(stars)as count,
            ROUND(avg(stars), 1) as avg FROM  $tableName  LEFT JOIN reviews ON products.id=reviews.user_id  GROUP by id");
        
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);

    }

          public function del($id)
        {
            $tableName = $this->tableName();
            $statement = self::prepare("DELETE FROM $tableName WHERE id=:$id");
            $statement->bindValue(":$id",$id);
             $statement->execute();
        }


    public function edit($id)
    {
        $tableName = $this->tableName();
        $statement = self::prepare("SELECT * FROM $tableName WHERE id=:$id");
        $statement->bindValue(":$id",$id);
        $statement->execute();
        return $statement->fetchAll(\PDO::FETCH_ASSOC);
    }

           public function show($id)
        {
            $statement = self::prepare("SELECT * FROM products WHERE id=:$id");
            $statement->bindValue(":$id",$id);
            $statement->execute();
             return $statement->fetchAll(\PDO::FETCH_ASSOC);
        }

        public function update($id)
        {
            $tableName = $this->tableName();
            $attributes = $this->attributes();
            $params = implode(',', array_map(fn($attr) => "$attr = :$attr",$attributes));
            $statement = self::prepare("UPDATE $tableName SET $params WHERE id=$id");
            foreach($attributes as $attribute){
                $statement->bindValue(":$attribute", $this->{$attribute});
            }
            $statement->execute();
            return true;
        }


        public static function prepare($sql)
        {
            return Application::$app->db->pdo->prepare($sql);

        }

}