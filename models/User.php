<?php

namespace app\models;
use app\core\DbModel;
use app\core\Model;
use app\core\UserModel;

class User extends UserModel
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_DELETED = 2;

    public string $firstname;
    public string $lastname;
    public int $status;
    public string $email;
    public string $age;
    public string $password;
    public string $confirmPassword;

    public function tableName():string
    {
        return 'users';
    }

    public function primaryKey():string
    {
        return 'id';
    }

    public function role():string
    {
        return 'status';
    }

    public function user():string
    {
        return 'firstname';
    }

    public function save()
    {
        if(empty($this->status)){
            $this->status = self::STATUS_INACTIVE;
        }
        $this->password = password_hash($this->password,PASSWORD_DEFAULT);
        return parent::save();
    }

    public function rules()
    {
        return [
            'firstname' => [self::RULE_REQUIRED],
            'lastname' => [self::RULE_REQUIRED],
            'email' => [self::RULE_REQUIRED,self::RULE_EMAIL,[
                self::RULE_UNIQUE,'class' => self::class
            ]],
            'age'=>[self::RULE_REQUIRED],
            'password' => [self::RULE_REQUIRED,[self::RULE_MIN,'min' => 8]],
            'confirmPassword' => [self::RULE_REQUIRED,[self::RULE_MATCH,'match' => 'password']],

        ];
    }

    public function attributes(): array
    {
      return ['firstname','lastname','status','email','age','password'];
    }

    public function getDisplayName():string
    {
        return $this->firstname.' '.$this->lastname;
    }

}