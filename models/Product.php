<?php

namespace app\models;
use app\core\DbModel;
use app\core\Model;

class Product extends DbModel
{
    public string $name;
    public string $description;
    public string $image;

  
 
    public function tableName():string
    {
        return 'products';
    }

    public function primaryKey():string
    {
        return 'id';
    }

   
    public function rules()
    {
        return [
            'name' => [self::RULE_REQUIRED],
            'description' => [self::RULE_REQUIRED],
            'image' => [self::RULE_REQUIRED],
          
        ];
    }

    public function attributes(): array
    {
      return ['name','description','image',];
    }

  

}