<?php

namespace app\models;
use app\core\DbModel;
use app\core\Model;


class Review extends DbModel
{
    public string $user_name;
    public string $product_name;
    public string $comment_text;
    public string $stars;
    public string $user_id;

 
    public function tableName():string
    {
        return 'reviews';
    }

    public function primaryKey():string
    {
        return 'id';
    }


    public function save()
    {
        if(empty($this->user_name)){
            $this->user_name = $_SESSION['action'];
            $this->product_name = $_GET['name'];
            $this->user_id = $_GET['id'];
        }
        return parent::save();
    }

    public function rules()
    {
        return [
            'user_name' => [self::RULE_REQUIRED],
            'product_name' => [self::RULE_REQUIRED],
            'comment_text' => [self::RULE_REQUIRED],
            'stars' => [self::RULE_REQUIRED],
            'user_id'=>[self::RULE_REQUIRED],
        ];
    }

    public function attributes(): array
    {
      return ['user_name', 'product_name', 'comment_text','stars','user_id'];
    }

  

}