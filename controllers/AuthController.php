<?php

namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\core\middlewares\AuthMiddleware;
use app\core\Request;
use app\core\Response;
use app\models\LoginForm;
use app\models\Review;
use app\models\User;
use app\models\Product;


class AuthController extends Controller
{
    public function __construct()
    {
        $this->registerMiddleware(new AuthMiddleware(['profile','update','delete','createProducts','createReviews','createUsers']));
    }

    public function login(Request $request, Response $response)
    {
        $loginForm = new LoginForm();
        if($request->isPost()){
            $loginForm->loadData($request->getBody());
            if($loginForm->login()){
                $response->redirect('/');
                return;

            }
        }
        return $this->render('login',[
        'error' => $loginForm->errors,
        ]);
    }

    public function register(Request $request)
    {
        $user = new User();
        if($request->isPost()){
            $user->loadData($request->getBody());
            if($user->emailUnique() && $user->validate() && $user->save()){
                Application::$app->session->setFlash('success','Thanks for registering');
                Application::$app->response->redirect('/');
            }

        }
        return $this->render('register',[
           'error' => $user->errors,
        ]);

    }

    public function profile()
    {
        $category = $_GET['category'];

        if($category == 'user'){
            $product = new User();
        }
        if($category == 'product'){
            $product = new Product();
        }
        if($category == 'review'){
            $product = new Review();
        }
        $product = $product->all();

        return $this->render('profile',[
            'products' => $product
        ]);
    }

    public function createUsers(Request $request)
    {
        $user = new User();
        if($request->isPost()){
            $user->loadData($request->getBody());

            if($user->emailUnique() && $user->validate() && $user->save()){
                Application::$app->session->setFlash('success','you made a new entry');
                Application::$app->response->redirect('/profile?category=user');
            }

        }
        return $this->render('create_user',[
            'errors' => $user->errors,
        ]);

    }

    public function createProducts(Request $request)
    {
        $product = new Product();
        if($request->isPost()){
            $product->loadData($request->getBody());
            if($product->validate() && $product->save()){

                Application::$app->session->setFlash('create','you made a new entry');
                Application::$app->response->redirect('/profile?category=user');
            }

        }
        return $this->render('create_product',[
            'errors' => $product->errors,
        ]);

    }

    public function createReviews(Request $request)
    {
        $review = new Review();
        if($request->isPost()) {
            $review->loadData($request->getBody());
            if ($review->validate() && $review->save()) {
                Application::$app->session->setFlash('create','you made a new entry');
                Application::$app->response->redirect('/profile?category=user');
            }
        }

        return $this->render('create_review',[
            'errors' => $review->errors,
        ]);

    }

    public function delete()
    {
        $category = $_GET['category'];
        $id = $_GET['id'];

        if($category == 'user'){
            $product = new User();
        }
        if($category == 'product'){
            $product = new Product();
        }
        if($category == 'review'){
            $product = new Review();
        }

         $product->del($id);
         Application::$app->session->setFlash('delete','you have successfully deleted');
         Application::$app->response->redirect('/profile?category=user');

    }

     public function update(Request $request)
    {
        $category = $_GET['category'];
        $id = $_GET['id'];

         if($category == 'user'){
             $product = new User();
         }
         if($category == 'product'){
             $product = new Product();
         }
         if($category == 'review'){
             $product = new Review();
         }

        if($request->isPost()){
            $product->loadData($request->getBody());
            if($product->validate() && $product->update($id)) {
                Application::$app->session->setFlash('update','you have successfully updated');
                Application::$app->response->redirect('/profile?category=user');
            }
        }
            $productId = $product->edit($id);

            return $this->render('update_'.$category,[
                'product' => $productId[0],
                'errors'=> $product->errors,
            ]);
    }

    public function logout(Request $request,Response $response)
    {
        Application::$app->logout();
        $response->redirect('/');
    }



}