<?php
namespace app\controllers;

use app\core\Application;
use app\core\Controller;
use app\core\Request;
use app\models\Product;
use app\models\Review;

class SiteController extends Controller
{
    public function home()
    {
        $products = new Product();

        $stars = $products->getAvg();
        $products = $products->all();
        return $this->render('home',[
            'products' => $products,
            'stars' => $stars,
        ]);
    }

    public function review(Request $request)
    {
        $products = new Product();
        $review = new Review();
        $name = $_GET['name'];
        $id = $_GET['id'];
        if($request->isPost()) {
            $review->loadData($request->getBody());
            if ($review->save()) {
                Application::$app->response->redirect('/');
            }
        }
        $productId = $products->show($id);
        return $this->render('review',[
            'product'=> $productId[0],
        ]);
    }

}