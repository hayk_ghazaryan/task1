<?php
use app\controllers\SiteController;
use app\controllers\AuthController;
use app\core\Application;

require_once __DIR__ . '/../vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

$config = [
    'userClass' =>\app\models\User::class,
    'db'=>[
        'dns' => $_ENV['DB_DNS'],
        'user' => $_ENV['DB_USER'],
        'password' => $_ENV['DB_PASSWORD'],
    ]
];

$app = new Application(dirname(__DIR__),$config);

$app->router->get('/',[SiteController::class,'home']);
$app->router->get('/review',[SiteController::class,'review']);
$app->router->post('/review',[SiteController::class,'review']);

$app->router->get('/login',[AuthController::class,'login']);
$app->router->post('/login',[AuthController::class,'login']);
$app->router->get('/register',[AuthController::class,'register']);
$app->router->post('/register',[AuthController::class,'register']);
$app->router->get('/logout',[AuthController::class,'logout']);

$app->router->get('/profile',[AuthController::class,'profile']);
$app->router->get('/create/user',[AuthController::class,'createUsers']);
$app->router->post('/create/user',[AuthController::class,'createUsers']);
$app->router->get('/create/product',[AuthController::class,'createProducts']);
$app->router->post('/create/product',[AuthController::class,'createProducts']);
$app->router->get('/create/review',[AuthController::class,'createReviews']);
$app->router->post('/create/review',[AuthController::class,'createReviews']);
$app->router->get('/profile/delete',[AuthController::class,'delete']);
$app->router->get('/profile/update',[AuthController::class,'update']);
$app->router->post('/profile/update',[AuthController::class,'update']);

$app->run();