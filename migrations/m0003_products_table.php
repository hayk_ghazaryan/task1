<?php

namespace app\migrations;

class m0003_products_table
{
    public function up()
    {
      $db = \app\core\Application::$app->db;
      $SQL = "CREATE TABLE products(
        id INT AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(255) NOT NULL,
        description VARCHAR(255) NOT NULL,
        image VARCHAR(255) NOT NULL,
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP)ENGINE=INNODB;";

      $db->pdo->exec($SQL);
    }

    public function down()
    {
        $db = \app\core\Application::$app->db;
        $SQL = "DROP TABLE products";
        $db->pdo->exec($SQL);
    }

}