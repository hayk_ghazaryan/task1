<?php

namespace app\migrations;

class m0004_reviews_table
{
    public function up()
    {
      $db = \app\core\Application::$app->db;
      $SQL = "CREATE TABLE reviews(
        id INT AUTO_INCREMENT PRIMARY KEY,
        user_name VARCHAR(255) NOT NULL,
        product_name VARCHAR(255) NOT NULL,
        comment_text VARCHAR(255) NOT NULL,
        stars VARCHAR(255) NOT NULL,
        user_id VARCHAR(255) NOT NULL, 
        created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP)ENGINE=INNODB;";

      $db->pdo->exec($SQL);
    }

    public function down()
    {
        $db = \app\core\Application::$app->db;
        $SQL = "DROP TABLE reviews";
        $db->pdo->exec($SQL);
    }

}